// setting up express and socket.io
var app = require('express')();
var fs = require('fs');

var pkey = fs.readFileSync('/etc/letsencrypt/live/chat.futuriumlab.eu/privkey.pem');
var pcert = fs.readFileSync('/etc/letsencrypt/live/chat.futuriumlab.eu/cert.pem')
var ca = fs.readFileSync('/etc/letsencrypt/live/chat.futuriumlab.eu/chain.pem');

var options = {
  key: pkey,
  cert: pcert,
  ca: ca
};

var https = require('https');
const http = https.createServer(options, app);

var io = require('socket.io')(http);
var Stomp = require('stompjs');

// starting the http server
http.listen(4000, function(){
  console.log('ready to go');
});

var users = {};
var sockets = {};

io.on('connection', function (socket) {

  socket.on('connected', function (data) {
    uid = data.uid;

    // If user already exists, add to users sockets array and the to sockets array.
    if (users[uid]) {
      users[uid].sockets.push(socket);
      sockets[socket.id] = uid;
    }
    // Otherwise create all the data.
    else {
      data.client = Stomp.overWS('ws://localhost:15674/ws');

      var on_connect = function(e) {
        console.log("Connected!");
        data.client.subscribe('/queue/user' + uid, function(d) {
          var message = JSON.parse(d.body);
          users[message.to].sockets.forEach(function(socket) {
            socket.emit('messageReceived', d.body);
          });
        });
      };
      var on_error = function(error) {
        console.log(error);
      };
      data.client.connect('client', 'client', on_connect, on_error, '/');

      sockets[socket.id] = uid;
      data.sockets = [socket];
      users[uid] = data;
    }
  });

  socket.on('sendMessage', function (data) {
    users[data.uid].client.send('/queue/user' + data.to, {"content-type":"application/json"}, JSON.stringify(data));
  });

  socket.on('disconnect', function() {
    if (sockets[socket.id]) {
      let user_id = sockets[socket.id];
      if (sockets[socket.id])
        delete sockets[socket.id];

      // Count sockets for this user, if = 1 delete him, otherwise, just delete
      // the socket.id entries.
      if (users[user_id] && users[user_id].sockets.length == 1) {
        delete users[user_id];
      }
      else {
        idx = users[user_id].sockets.indexOf(socket);
        users[user_id].sockets.splice(idx, 1);
      }
    }
  });
});
