// Stomp.js
importScripts('/modules/custom/messaging/js/stomp.js')

// shared-worker.js
const connections = [];
onconnect = function(e) {

  const port = e.ports[0];
  connections.push(port);
  var client = null;

  port.onmessage = function(e) {
    var data = JSON.parse(e.data);

    if (data.cmd) {
      switch(data.cmd) {
        case 'init':
          if (client == null) {
            // Initialize the stomp client.
            client = Stomp.client(data.url);
            client.debug = function(str) {};

            // Connect callback.
            var on_connect = function(x) {
              console.log("STOMP Connected!");
              // Start the consumer.
              client.subscribe('/amq/queue/user' + data.uid, function(d) {
                message = JSON.parse(d.body);
                connections.forEach(function(connection) {
                  connection.postMessage(JSON.stringify({cmd: 'messageReceived', msg: message}));
                });
              });
            }

            // Error callback.
            var on_error =  function(e) {
              console.log('STOMP Error!');
            };

            client.connect('guest', 'guest', on_connect, on_error, '/');
          }
          break;

        case 'sendMessage':
          // Send a message.
          if (data.cmd == 'sendMessage' && data.message) {
            client.send('/amq/queue/user' + data.message.to, {"content-type":"application/json"}, JSON.stringify(data.message));
          }
          break;
      }
    }
  };

};
