(function ($, Drupal, drupalSettings) {

  if (isMobileDevice()) {
    $('html').addClass('mobile');
  }

  $(window).bind('load', function() {
    var settings = drupalSettings.messaging;
    if (settings.user.uid && settings.user.uid != 0) {

      // Keep workers disabled for now.
      settings.useWorker = false;
      if (typeof SharedWorker != 'undefined' && settings.useWorker) {
        // Use SharedWorker when available.
        var myWorker = new SharedWorker(settings.path.base + '/' + settings.path.module + '/js/worker.js');
        myWorker.port.start();
        myWorker.port.postMessage(JSON.stringify({cmd: 'init', uid: settings.user.uid, url: 'wss://chat.europa.eu:15674/ws'}));

        myWorker.port.onmessage = function(e) {
          data = JSON.parse(e.data);
          if (data.cmd == 'messageReceived') {
            $().newChatBox(data.msg.from, data.msg);
          }
        }

        $.fn.chatMessage = function(message) {
          var msg_string = JSON.stringify({cmd: 'sendMessage', message: message});
          myWorker.port.postMessage(msg_string);
        }
      }
      else {
        // Fallback if shared worker not available.
        var wall = io(settings.server);
        wall.emit('connected', settings.user);

        wall.on('messageReceived', function(data) {
          parsed = JSON.parse(data);
          $().newChatBox(parsed.from);
        });

        $.fn.chatMessage = function(message) {
          wall.emit('sendMessage', message);
        }
      }

      $('.chat-list .chat-user').click(function(e) {
        uid = $(this).attr('uid');
        if (!$('.chat-box[uid="'+uid+'"]').length) {
          $().newChatBox(uid, null, true);
        }
        else {
          $('.chat-box[uid="'+uid+'"]').find('textarea').focus();
        }

/*
        current = parseInt($('.chat-boxes-wrapper').css('top'));
        offset = $('.chat-box[uid="'+uid+'"]').offset().top;

        $('.chat-boxes-wrapper').animate({
            top: current - offset
        });
*/
      });

      $.fn.appendMessage = function(to, rendered_message) {
        // Add To chat box.
        div = $('.chat-box[uid="'+to+'"] .body');
        div.append(rendered_message);
        $().scrollChatBox(to);
      }

      $.fn.scrollChatBox = function(to) {
        // Add To chat box.
        div = $('.chat-box[uid="'+to+'"] .body-outer');
        div.scrollTop(div.prop("scrollHeight"));

        $('.chat-box[uid="'+to+'"] .body .message:not(".own")').each(function() {
          if ($(this).attr('author') == settings.user.uid) {
            $(this).addClass('own');
          }
        });
      }

      $.fn.newChatBox = function(uid, message, getFocus = false) {

        if (!$('.chat-box[uid="'+uid+'"]').length) {
          $('.chat-boxes-wrapper').append(
            '<div uid="'+uid+'" class="chat-box"> \
              <div class="header"></div>          \
              <div class="body-outer">            \
                <div class="body"></div>          \
              </div>                              \
              <div class="footer"></div>          \
            </div>'
          );
          $('.chat-box[uid="'+uid+'"]').slideToggle(function() {
            $(this).find('textarea').focus();
          });

          $('.chat-box[uid="'+uid+'"] .body-outer').on('mousewheel DOMMouseScroll', function ( e ) {
            var e0 = e.originalEvent,
                delta = e0.wheelDelta || -e0.detail;

            this.scrollTop += ( delta < 0 ? 1 : -1 ) * 30;
            e.preventDefault();
          });

        }
        else {
          $('.chat-box[uid="'+uid+'"]').find('textarea').focus();
        }

        $.ajax({
          url: settings.path.base + '/messaging/messages/user/' + settings.user.uid + '/user/' + uid,
          context: document.body
        }).done(function(messages) {
          decoded = decode_entities(messages);
          $('.chat-box[uid="'+uid+'"] .body').html(decoded);
          if (message) {
            $('.chat-box[uid="'+uid+'"] .body').append(message);
          }
          $().scrollChatBox(uid);
        });

        $('.chat-box[uid="'+uid+'"] .header').html($('.chat-list div[uid="'+uid+'"]').clone());

        form = $('<textarea placeholder="Write your message here" rows="1"></textarea>');
        form.keypress(function(e) {
          if(e.which == 10 || e.which == 13) {
            e.preventDefault();
            uid = $(this).closest('.chat-box').attr('uid');
            msg = $(this).val();

            $('#message-form form').find('input[name="target[entity_type]"]').val('user');
            $('#message-form form').find('input[name="target[entity_id]"]').val(uid);
            $('#message-form form').find('textarea[name="message"]').val(msg)
            $('#message-form form').find('button').trigger('mousedown');

            $(this).val('');
          }
        });
        $('.chat-box[uid="'+uid+'"] .footer').html(form);

        $('.chat-box .header').click(function(e) {
          $(this).closest('.chat-box').slideToggle(function() {
            $(this).remove();
          });
        });
      }

/*
      $('a.profile__actions--contact').click(function(e) {
        e.preventDefault();
        url = $(this).attr('href');
        uid = url.replace('/profile/','');
        uid = uid.replace('/contact','');
        if (uid != settings.user.uid) {
          $().newChatBox(uid);
        }
      });
*/

      setTimeout(function() {
        item = $('<li class="navigation-menu__item chat"><a href=""><i class="fas fa-comment-alt"></i><span class="no-display--sm">Chat</span></a></li>');

        item.click(function(e) {
          e.preventDefault();
          $('.chat-list').toggle();
        });

        item.insertAfter('.navigation-menu__item.events');
      }, 10);


      $('.chat-list').on('mousewheel DOMMouseScroll', function ( e ) {
        var e0 = e.originalEvent,
            delta = e0.wheelDelta || -e0.detail;

        this.scrollTop += ( delta < 0 ? 1 : -1 ) * 30;
        e.preventDefault();
      });

    }

  });

  function isMobileDevice() {
    if( navigator.userAgent.match(/Android/i)
    || navigator.userAgent.match(/webOS/i)
    || navigator.userAgent.match(/iPhone/i)
    || navigator.userAgent.match(/iPad/i)
    || navigator.userAgent.match(/iPod/i)
    || navigator.userAgent.match(/BlackBerry/i)
    || navigator.userAgent.match(/Windows Phone/i)
    ){
       return true;
     }
    else {
       return false;
     }
  };

  var decode_entities = (function() {
      // Remove HTML Entities
      var element = document.createElement('div');

      function decode_HTML_entities (str) {

          if(str && typeof str === 'string') {

              // Escape HTML before decoding for HTML Entities
              str = escape(str).replace(/%26/g,'&').replace(/%23/g,'#').replace(/%3B/g,';');

              element.innerHTML = str;
              if(element.innerText){
                  str = element.innerText;
                  element.innerText = '';
              }else{
                  // Firefox support
                  str = element.textContent;
                  element.textContent = '';
              }
          }
          return unescape(str);
      }
      return decode_HTML_entities;
  })();

})(jQuery, Drupal, drupalSettings);
