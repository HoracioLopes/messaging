<?php

namespace Drupal\messaging\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\AppendCommand;
use Symfony\Component\HttpFoundation\Response;
use Drupal\Core\Access\AccessResult;

/**
 * Front page controller.
 */
class MessagingFormController extends ControllerBase {

  public function messagesAccess($source_entity = NULL, $source = NULL, $target_entity = NULL, $target = NULL) {
    $current_user = \Drupal::currentUser()->id();
    if ($current_user == $source || $current_user == $target) {
      return AccessResult::allowed();
    }
    return AccessResult::forbidden();
  }

  public function messages($source_entity = NULL, $source = NULL, $target_entity = NULL, $target = NULL) {
    $db = \Drupal::database();
    $query = $db->select('messaging','m')
      ->fields('m');

    $and1 = db_and()
      ->condition('entity_from', "%" . $db->escapeLike('user') . "%", 'LIKE')
      ->condition('from', "%" . $db->escapeLike($source) . "%", 'LIKE')
      ->condition('entity_to', "%" . $db->escapeLike('user') . "%", 'LIKE')
      ->condition('to', "%" . $db->escapeLike($target) . "%", 'LIKE');

    $and2 = db_and()
      ->condition('entity_from', "%" . $db->escapeLike('user') . "%", 'LIKE')
      ->condition('from', "%" . $db->escapeLike($target) . "%", 'LIKE')
      ->condition('entity_to', "%" . $db->escapeLike('user') . "%", 'LIKE')
      ->condition('to', "%" . $db->escapeLike($source) . "%", 'LIKE');

    $or = db_or()
      ->condition($and1)
      ->condition($and2);

    $data = $query
      ->condition($or)
      ->orderBy('date', 'DESC')
      ->range(0, 15)
      ->execute()
      ->fetchAll();

    $messages = array_reverse($data);

    foreach ($messages as $message) {
      $output[] = [
        '#theme' => 'messaging',
        '#message' => $message,
      ];
    }

    $html = \Drupal::service('renderer')->renderRoot($output);
    $response = new Response();
    $response->setContent($html);
    return $response;
  }

}
