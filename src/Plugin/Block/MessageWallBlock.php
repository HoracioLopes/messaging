<?php

namespace Drupal\messaging\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Message Wall' Block.
 *
 * @Block(
 *   id = "message_wall_block",
 *   admin_label = @Translation("Message Wall"),
 *   category = @Translation("Messaging"),
 * )
 */
class MessageWallBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $params = \Drupal::routeMatch()->getParameters()->all();
    $param = array_pop($params);
    $output = [];
    if (!empty($param) && $param->getEntityTypeId() == 'group') {
      $db = \Drupal::database();
      $query = $db->select('messaging','m')
        ->fields('m');

      $data = $query
        ->condition('entity_to', "%" . $db->escapeLike($param->getEntityTypeId()) . "%", 'LIKE')
        ->condition('to', "%" . $db->escapeLike($param->id()) . "%", 'LIKE')
        ->orderBy('date', 'DESC')
        ->range(0, 10)
        ->execute()
        ->fetchAll();

      $messages = array_reverse($data);

      foreach ($messages as $message) {
        $output[] = [
          '#theme' => 'messaging',
          '#message' => $message,
        ];
      }
    }
    return $output;
  }

}
