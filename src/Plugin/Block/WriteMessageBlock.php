<?php

namespace Drupal\messaging\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Write Message' Block.
 *
 * @Block(
 *   id = "write_message_block",
 *   admin_label = @Translation("Write Message"),
 *   category = @Translation("Messaging"),
 * )
 */
class WriteMessageBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    /*
    $form = \Drupal::formBuilder()->getForm('Drupal\messaging\Form\MessageForm');
    return (isset($form['source']))
      ? $form
      : [];
      */
  }

}
