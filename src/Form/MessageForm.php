<?php
/**
 * @file
 * Contains \Drupal\messaging\Form\MessageForm.
 */

namespace Drupal\messaging\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\InvokeCommand;

class MessageForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'message_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $current_user = \Drupal::currentUser();
    $form_values = $form_state->getValues();

    $form['#prefix'] = '<div id="message-form">';
    $form['#suffix'] = '</div>';

    $form['source'] = [
      '#tree' => TRUE,
    ];

    $form['source']['entity_type'] = [
      '#type' => 'hidden',
      '#value' => 'user',
    ];

    $form['source']['entity_id'] = [
      '#type' => 'hidden',
      '#value' => $current_user->id(),
    ];

    $form['target'] = [
      '#tree' => TRUE,
    ];

    $form['target']['entity_type'] = [
      '#type' => 'hidden',
      '#value' => $form_values['target']['entity_type'] ?? 0,
    ];

    $form['target']['entity_id'] = [
      '#type' => 'hidden',
      '#value' => $form_values['target']['entity_id'] ?? 0,
    ];

    $form['message'] = [
      '#type' => 'textarea',
      '#placeholder' => $this->t("Write your message here"),
      '#rows' => 1,
    ];

    $form['actions'] = [
      '#prefix' => '<div class="hidden">',
      '#suffix' => '</div>',
      '#type' => 'button',
      '#id' => 'send-message',
      '#value' => $this->t('Send'),
      '#ajax' => [
        'callback' => '::sendMessage',
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function sendMessage(array &$form, FormStateInterface $form_state) {

    $message = new \stdClass();

    $message->entity_from = $form_state->getValue('source')['entity_type'];
    $message->from = $form_state->getValue('source')['entity_id'];

    $input = $form_state->getUserInput();

    $message->entity_to = $input['target']['entity_type'];
    $message->to = $input['target']['entity_id'];;

    // @todo: needed?
    $message->uid = $form_state->getValue('source')['entity_id'];
    $message->date = time();
    $message->text = check_markup($form_state->getValue('message'), 'chat_html');
    $message->status = 1;

    // Using a merge query in a custom table.
    $message->twid = \Drupal::database()->insert('messaging')
      ->fields([
        'entity_from' => $message->entity_from,
        'from' => $message->from,
        'entity_to' => $message->entity_to,
        'to' => $message->to,
        'uid' => $message->uid,
        'date' => $message->date,
        'text' => $message->text,
        'status' => $message->status,
      ])
      ->execute();

    $response = new AjaxResponse();
    $response->addCommand(new InvokeCommand('#message-form textarea', 'val', ['']));

    $output[] = [
      '#theme' => 'messaging',
      '#message' => $message,
    ];
    $html = \Drupal::service('renderer')->renderRoot($output);

    switch ($message->entity_to) {
      case 'user':
        $response->addCommand(new InvokeCommand(NULL, 'appendMessage', [$message->to, $html]));
        $response->addCommand(new InvokeCommand(NULL, 'chatMessage', [$message]));
        break;
    }

    return $response;

  }
}
